$(document).ready(function(){
	
	$('#price-range-submit').hide();

	$("input[name='min'],input[name='max']").on('change', function () {

	  $('#price-range-submit').show();

	  var min_price_range = parseInt($("input[name='min']").val());

	  var max_price_range = parseInt($("input[name='max']").val());

	  if (min_price_range > max_price_range) {
		$("input[name='max']").val(min_price_range);
	  }

	  $("#slider-range").slider({
		values: [min_price_range, max_price_range]
	  });
	  
	});


	$("input[name='min'],input[name='max']").on("paste keyup", function () {

	  $('#price-range-submit').show();

	  var min_price_range = parseInt($("input[name='min']").val());

	  var max_price_range = parseInt($("input[name='max']").val());
	  
	  if(min_price_range == max_price_range){

			max_price_range = min_price_range + 100;
			
			$("input[name='min']").val(min_price_range);		
			$("input[name='max']").val(max_price_range);
	  }

	  $("#slider-range").slider({
		values: [min_price_range, max_price_range]
	  });

	});


	/*$(function () {
	  $("#slider-range").slider({
		range: true,
		orientation: "horizontal",
		min: 2000,
		max: 3000,
		values: [0, 10000],
		step: 100,

		slide: function (event, ui) {
		  if (ui.values[0] == ui.values[1]) {
			  return false;
		  }
		  
		  $("#min_price").val(ui.values[0]);
		  $("#max_price").val(ui.values[1]);
		}
	  });

	  $("#min_price").val($("#slider-range").slider("values", 0));
	  $("#max_price").val($("#slider-range").slider("values", 1));

	});*/

	$("#slider-range,#price-range-submit").click(function () {

	  var min_price = $('#min_price').val();
	  var max_price = $('#max_price').val();

	  $("#searchResults").text("Here List of products will be shown which are cost between " + min_price  +" "+ "and" + " "+ max_price + ".");
	});
	
	
	//second ranger
	$("input[name='mobile_min'],input[name='mobile_max']").on('change', function () {

	  $('#price-range-submit').show();

	  var min_price_range = parseInt($("input[name='mobile_min']").val());

	  var max_price_range = parseInt($("input[name='mobile_max']").val());

	  if (min_price_range > max_price_range) {
		$("input[name='max']").val(min_price_range);
	  }

	  $("#slider-range2").slider({
		values: [min_price_range, max_price_range]
	  });
	  
	});


	$("input[name='mobile_min'],input[name='mobile_max']").on("paste keyup", function () {

	  $('#price-range-submit').show();

	  var min_price_range = parseInt($("input[name='mobile_min']").val());

	  var max_price_range = parseInt($("input[name='mobile_max']").val());
	  
	  if(min_price_range == max_price_range){

			max_price_range = min_price_range + 100;
			
			$("input[name='mobile_min']").val(min_price_range);		
			$("input[name='mobile_max']").val(max_price_range);
	  }

	  $("#slider-range2").slider({
		values: [min_price_range, max_price_range]
	  });

	});

});